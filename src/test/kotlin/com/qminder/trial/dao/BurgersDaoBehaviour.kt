package com.qminder.trial.dao

import org.junit.Assert.assertEquals
import org.junit.Test

class BurgersDaoBehaviour {

    @Test
    fun shouldGetBurgerPlaces() {
        val limit = 10
        val places = getBurgerPlaces(limit, 0);
        assertEquals(limit, places.size)
    }
}