var burgersOffset = 0;
// Reasonably large limit, so that the page could be filled
var burgersLimit = 6;

var addingBurgers = false;

function addError() {
    $("#burgers").append('<div class="error">' +
            '<p>Failed to fetch burgers. Try to refresh the page.</p>' +
            '<p>If it continues to fail, connect the system administrator.</p>' +
    '</div>');
}

function addBurger(burger, parity) {
    $("#burgers").append('<div class="burger-info ' + parity + '">' +
            '<table>' +
            '<tr>' +
            '<td>' +
            '<img src="' + burger.img + '" alt="' + burger.name +'" align="right">' +
            '</td>' +
            '<td>' +
            '<h2>' + burger.name + '</h2><p>Address: <strong>' + burger.address + '</strong></p>' +
            '</td>' +
            '</tr>' +
            '</table>' +
            '</div>');
}

function handleResponse(response, offset) {
    var even = false;
    response.forEach(function(burger) {
        var parity = even ? "even" : "odd"
        addBurger(burger, parity);
        even = !even
    });
}

function addNextBurgers() {
    if (addingBurgers) {
        return;
    }
    addingBurgers = true;
    hasMoreBurgers = true

    burgerParams = {
        limit: burgersLimit,
        offset: burgersOffset
    }
    $.ajax({
        url: "burgers",
        data: burgerParams,
        success: function(burgersResponse) {
            if (burgersResponse.length == 0) {
                hasMoreBurgers = false;
                return;
            }
            handleResponse(burgersResponse, burgerParams.offset)
        },
        dataType: "json",
        error: function(err) {
            addError();
            addingBurgers = false;
        }
    });

    burgersOffset += burgersLimit;
    addingBurgers = false;
    return hasMoreBurgers;
}

$(document).ready(function() {
    addNextBurgers();
});

$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        addNextBurgers();
    }
});