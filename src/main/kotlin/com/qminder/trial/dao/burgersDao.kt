package com.qminder.trial.dao

import com.qminder.trial.domain.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

const val IMG_DEFAULT_DIR = "img-default"
const val NOT_FOUND_IMG = "$IMG_DEFAULT_DIR/NOT_FOUND.png"

val log: Logger = LoggerFactory.getLogger("burgers-dao")

/**
 * Get all the burger places
 *
 * @param limit - maximum number of entries to be returned
 * @param offset - how many first entries omitted
 *
 * @return list of all burger places
 */
fun getBurgerPlaces(limit: Int, offset: Int): List<BurgerPlace> {
    var result: List<BurgerPlace>
    runBlocking {
        getHttpClient().use { client ->
            val responseBody = client.get<BurgersResponseBody> {
                url("https://api.foursquare.com/v2/venues/explore")
                contentType(ContentType.Application.Json)

                for((key, value) in getFoursquareParameters()) {
                    parameter(key, value)
                }

                parameter("near", "Tartu,EE")
                parameter("query", "burger")
                parameter("limit", limit)
                parameter("offset", offset)
            }
            result = processBurgerPlacesResponse(responseBody)
            addBurgerImages(result)
        }
    }

    return result
}

private fun getHttpClient(): HttpClient {
    return HttpClient(Apache) {
        install(JsonFeature)
    }
}

private fun getFoursquareParameters(): Map<String, String> {
    return mapOf(
        "client_id" to System.getenv("FOURSQUARE_CLIENT_ID"),
        "client_secret" to System.getenv("FOURSQUARE_CLIENT_SECRET"),
        "v" to "20180323"
    )
}

private fun processBurgerPlacesResponse(responseBody: BurgersResponseBody): List<BurgerPlace> {
    val result = ArrayList<BurgerPlace>()
    for (eachGroup in responseBody.response.groups) {
        for (eachItem in eachGroup.items) {
            result.add(eachItem)
        }
    }
    return result
}

private fun addBurgerImages(burgerPlaces: List<BurgerPlace>) {
    burgerPlaces.parallelStream().forEach {
        it.img = getBurgerImage(it.venue) ?: getDefaultBurgerImage(it.venue)
    }
}

private fun getBurgerImage(venue: Venue): String? {
    try {
        val imageUrls = getPotentialImageUrls(venue.id) ?: return null
        return getImageWithBurgerDisplayed(imageUrls)
    } catch (e: Exception) {
        log.warn("Failed to fetch burger image for venue '${venue.name}', falling back to default", e)
        return null
    }
}

data class RecognizeRequestBody(val urls: List<String>)

private fun getImageWithBurgerDisplayed(imageUrls: List<String>): String? {
    val requestBody = RecognizeRequestBody(imageUrls)
    var result: String?
    runBlocking {
        getHttpClient().use { client ->
            val responseBody = client.post<RecognizeResponseBody> {
                url("https://pplkdijj76.execute-api.eu-west-1.amazonaws.com/prod/recognize")
                contentType(ContentType.Application.Json)

                body = requestBody
            }
            result = responseBody.urlWithBurger
        }
    }
    return result
}

private fun getPotentialImageUrls(venueId: String): List<String>? {
    var result: List<String>?
    runBlocking {
        getHttpClient().use { client ->
            val responseBody = client.get<PhotosResponseBody> {
                url("https://api.foursquare.com/v2/venues/$venueId/photos")
                contentType(ContentType.Application.Json)

                for((key, value) in getFoursquareParameters()) {
                    parameter(key, value)
                }
            }
            result = processPhotoItemsResponse(responseBody)
        }
    }
    return result
}

private fun processPhotoItemsResponse(body: PhotosResponseBody): List<String>? {
    if(body.meta.code != 200) {
        return null
    }
    return body.response.photos.items.map { it.getFullUrl() }
}

private fun getDefaultBurgerImage(venue: Venue): String {
    return getBurgerImageFromResources(venue) ?: NOT_FOUND_IMG
}

// XXX: Can it be rewritten without these warnings being suppressed?
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
private fun getBurgerImageFromResources(venue: Venue): String?  {
    val foundFile = File("src/main/resources/$IMG_DEFAULT_DIR").listFiles()
        .map { it.name }.firstOrNull { it.startsWith(venue.id) } ?: return null

    return "$IMG_DEFAULT_DIR/$foundFile"
}
