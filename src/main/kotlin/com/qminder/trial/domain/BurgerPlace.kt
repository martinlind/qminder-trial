package com.qminder.trial.domain

data class BurgerPlace(val venue: Venue, val referralId: String) {
    var img = ""

    fun getBasicInfo(): Map<String, String> {
        return mapOf(
            "name" to venue.name,
            "address" to getHumanAddress(),
            "img" to img)
    }

    // Useless call warning suppressed, because without filtering gives NPE and
    // therefore 500 - Internal Server Error
    @Suppress("UselessCallOnCollection")
    private fun getHumanAddress(): String {
        val location = venue.location
        val elements = listOf(
            location.address,
            location.postalCode,
            location.city,
            location.country)

        return elements.filterNotNull().joinToString(" ")
    }
}

data class Location(
    val address:String,
    val cc:String,
    val city:String,
    val country:String,
    val lat:String,
    val lng:String,
    val postalCode:String,
    val state:String,
)

data class Venue(val id: String, val name: String, val location: Location)
