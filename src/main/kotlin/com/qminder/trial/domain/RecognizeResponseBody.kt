package com.qminder.trial.domain

data class RecognizeResponseBody(val urlWithBurger: String)
