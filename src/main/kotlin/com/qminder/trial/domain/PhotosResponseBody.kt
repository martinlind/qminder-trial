package com.qminder.trial.domain

data class PhotosResponseBody(val meta: PhotosMeta, val response: PhotosResponse)

data class PhotosMeta(val code: Int)

data class PhotosResponse(val photos: Photos)

data class Photos(val count: Int, val items: List<PhotoItem>)

data class PhotoItem(val height: Int, val width: Int, val prefix: String, val suffix: String) {
    fun getFullUrl(): String{
        return "${prefix}${height}x$width${suffix}"
    }
}
