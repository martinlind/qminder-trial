package com.qminder.trial.domain

data class BurgersResponseBody(val meta: Meta, val response: BurgersResponse)

data class BurgersResponse(val geocode: Geocode, val groups: List<Group>)

data class Group(val items: List<BurgerPlace>)

data class Geocode(val cc: String)

data class Meta(val code: Int, val requestId: String)
