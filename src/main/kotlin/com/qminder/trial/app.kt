package com.qminder.trial

import com.qminder.trial.dao.IMG_DEFAULT_DIR
import com.qminder.trial.dao.getBurgerPlaces
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.lang.Exception

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 23567
    embeddedServer(Netty, port) {
        install(ContentNegotiation) {
            gson {
                setPrettyPrinting()
            }
        }
        routing {
            static("static") {
                resources("js")
                resources("css")
                resources(IMG_DEFAULT_DIR)
            }
            static(IMG_DEFAULT_DIR) {
                resources(IMG_DEFAULT_DIR)
            }

            resource("/", "index.html")
            resource("*", "index.html")

            get("burgers") {
                val limit = call.parameters["limit"]?.toIntOrNull() ?: 10
                val offset = call.parameters["offset"]?.toIntOrNull() ?: 0
                try {
                    val burgerPlaces = getBurgerPlaces(limit, offset)
                    val response = mutableListOf<Map<String,String>>()
                    burgerPlaces.mapTo(response) {it.getBasicInfo()}
                    call.respond(response)
                } catch (e: Exception) {
                    log.error("Error occurred when fetching burgers: ${e.message}", e)
                    call.respond(
                        status = HttpStatusCode.InternalServerError,
                        message = "Failed to fetch burgers")
                }
            }
        }
    }.start(wait = true)
}