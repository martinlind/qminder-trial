# Qminder - Test Assignment #

[Description](http://careers.qminder.com/kotlin-pro-assignment/)

The application is about getting to know the new burger venues in Tartu fast.

[The Application](https://qminder-trial-martinlind.herokuapp.com/)

### Setting Up Development Environment ###

The following **environment variables** need to be set up (for example in _Run Configurations_ of IntelliJ IDEA):

```bash
FOURSQUARE_CLIENT_ID=<your-foursquare-client-id>
FOURSQUARE_CLIENT_SECRET=<your-foursquare-client-secret>
```

TODO: how exactly to set it up?

### Deploying To Production ###

**Build** the deployable application with Gradle:

```
gradle stage
```

Deploy the app via Heroku:

```
heroku login
heroku create qminder-trial-martinlind
git push heroku master

```

If an app is **already running**: **destroy** it before deploy:

```
heroku apps:destroy qminder-trial-martinlind
```

### Additional Tips ###

If you want to open the file `Qminder_MASTER.mm`, you need [Freeplane](https://www.freeplane.org/wiki/index.php/Home) (version 1.8 or newer) for it.
